const options = {
  hidefeed: true,
  seenlikes: true,
  hidelikes: true,
  hidesidebar: true,
  hidechat: true,
  hidepinned: true,
  hidebanner: true,
  hidelikebutton: true
};

chrome.storage.sync.get(options, function(items) {
 Object.keys(items).forEach(function(item){
  if(item === "seenlikes" && items[item] === true) {
    setInterval(markUninterestedNotifsRead, 2000);
  } else if (items[item]) {
    var link = document.createElement("link");
    link.type = "text/css";
    link.rel = "stylesheet";
    link.href = chrome.extension.getURL("css/"+item+".css");
    document.getElementsByTagName("head")[0].appendChild(link);
  }
})

});

function markUninterestedNotifsRead() {
  var selectorString = `ul:not([data-gt*='"ref":"notif_page"']) li[data-gt*='"notif_type":"like"'] ._55m9._55ma._5c9q,
  ul:not([data-gt*='"ref":"notif_page"']) li[data-gt*='"notif_type":"like_tagged"'] ._55m9._55ma._5c9q,
  ul:not([data-gt*='"ref":"notif_page"']) li[data-gt*='"notif_type":"feedback_reaction_generic"'] ._55m9._55ma._5c9q,
  ul:not([data-gt*='"ref":"notif_page"']) li[data-gt*='"notif_type":"feedback_reaction_generic_tagged"'] ._55m9._55ma._5c9q,
  ul:not([data-gt*='"ref":"notif_page"']) li[data-gt*='"notif_type":"page_new_likes"'] ._55m9._55ma._5c9q,
  ul:not([data-gt*='"ref":"notif_page"']) li[data-gt*='"notif_type":"page_user_activity"'] ._55m9._55ma._5c9q,
  ul:not([data-gt*='"ref":"notif_page"']) li[data-gt*='"notif_type":"open_graph_action_like"'] ._55m9._55ma._5c9q,
  ul:not([data-gt*='"ref":"notif_page"']) li[data-gt*='"notif_type":"page_fan"'] ._55m9._55ma._5c9q,
  ul:not([data-gt*='"ref":"notif_page"']) li[data-gt*='"subtype":"highlights_friend_liker_commenter"'] ._55m9._55ma._5c9q`

  var fnSendClickEvent = function (elm) {
    var evt = document.createEvent("MouseEvent");
    evt.initUIEvent("click", true, true);
    elm.dispatchEvent(evt);
  }
  document.querySelectorAll(selectorString).forEach(fnSendClickEvent);
}
