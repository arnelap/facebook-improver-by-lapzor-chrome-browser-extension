const options = {
  hidefeed: true,
  seenlikes: true,
  hidelikes: true,
  hidesidebar: true,
  hidechat: true,
  hidepinned: true,
  hidebanner: true,
  hidelikebutton: true
};

function save_options() {
  Object.keys(options).forEach(function(item){
    options[item] = document.getElementById(item).checked
  })
  chrome.storage.sync.set(options, function() {
    document.querySelector('.mdl-js-snackbar').MaterialSnackbar.showSnackbar({message: 'Options saved.'});
  });
}
document.getElementById('save').addEventListener('click', save_options);

function restore_options() {
  chrome.storage.sync.get(options, function(items) {
    Object.keys(items).forEach(function(item){
      items[item] && document.getElementById(item).parentElement.MaterialSwitch.on()
    });

  });
}
document.addEventListener('DOMContentLoaded', restore_options);